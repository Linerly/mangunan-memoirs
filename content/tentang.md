---
title: Tentang
date: 2024-03-01T01:29:32.000Z
---

*Mangunan Chronicles* adalah sebuah tempat di mana siswa-siswa dari SMP Eksperimental Mangunan bisa menyediakan konten mereka -- seperti artikel, cerita, pengalaman, dan lain-lain -- di sini.

Ini merupakan proyek [bersumber terbuka](https://gitlab.com/Linerly/mangunan-chronicles) dari kelompok 2 (Alle, Edo, Yuda, Yudhis, dan Michael) di Komunitas Minat & Bakat IT.

{{< collapse summary="*Punya sesuatu keren untuk ditampilkan di sini?*" content="Kamu bisa menghubungi Michael (9C) melalui surel (*e-mail*) di [mangunanchronicles@linerly.xyz](mailto:mangunanchronicles@linerly.xyz). Setelah dikirim, kontenmu akan disunting lebih lanjut dan tautan (*link*) ke hasilnya akan dikirimkan." >}}

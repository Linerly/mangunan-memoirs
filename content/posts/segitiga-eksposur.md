---
title: Segitiga Eksposur
date: 2024-03-15T01:27:00.000Z
featured_image: /img/fotis-fotopoulos-rk3ymracwtc-unsplash.jpg
featured_image_attribution: Foto oleh <a
  href="https://unsplash.com/@ffstop?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Fotis
  Fotopoulos</a> dari <a
  href="https://unsplash.com/photos/time-lapse-photography-of-road-during-night-time-rK3YMRAcWtc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
draft: false
author: Alle
class: 7A
profile: https://www.instagram.com/filemkebakaran
profile_platform: instagram
---
Segitiga eksposur adalah suatu elemen yang mengontrol sebuah foto dan merujuk pada 3 elemen lagi, yaitu ISO, *Aperture*, dan *Shutter Speed*; simak selanjutnya untuk pembahasannya!

# ISO

Istilah ini merujuk pada seberapa sensitif sebuah kamera kepada cahaya, semakin tinggi ISO-nya, semakin sensitif yang cocok untuk dipakai di kondisi *low-light* atau *indoor*. Semakin rendah ISO-nya, semakin rendah kesensitifannya terhadap cahaya sehingga cocok untuk dipakai dalam kondisi cerah atau *outdoor*.

ISO memiliki jangkauan dari 25-6400 untuk model kamera baru, bahkan beberapa bisa mencapai ISO 32000 tergantung kameranya.

# *Aperture*

Apakah kalian pernah melihat *background* dari beberapa foto *blur* (buram), atau jernih tidak *blur*? Nah, itu disebabkan oleh elemen *Aperture* yang mengontrol seberapa masuk cahaya ke lensa dan kamera.

Semakin rendah *aperture*-nya, semakin *blur* *background*-nya. Untuk kebalikannya, semakin tinggi *aperture*-nya, semakin jernih fotonya atau *background*, alhasil memerlukan cahaya yang cukup banyak.

*Aperture* tersendiri digunakan tergantung kondisi cahaya pada suatu tempat. Jika gelap, maka pakailah *aperture* serendah mungkin sehingga banyak cahaya yang masuk ke dalam. Kebalikannya, jika dalam kondisi cerah/terang bisa memakai *aperture* yang tinggi, atau jika masih mau pakal yang rendah perlu diatur ISO dan *Shutter Speed*-nya.

# *Shutter Speed*

Elemen terakhir ini mengatur seberapa cahaya yang masuk pada sensor/kertas film (khusus analog) pada sebuah kamera, *shutter* ini berbentuk seperti gorden pada jendela yang jika aktif akan membuka gordennya dan menutup kembali.

*Shutter* bisa diatur sesuai kondisi cahaya juga dan sesuai *aperture*. Jika kondisi sedang dalam *low-light*, maka pakai *aperture* serendah mungkin dan shutter yang rendah atau sedang jika dibutuhkan. Sebaliknya, jika kondisi cerah atau terang, maka disarankan memakai *shutter speed* yang cepat seperti 1/500 atau 1/2000. *Shutter speed* memiliki jangkauan dari *bulb* hingga 1/4000 bahkan 1/8000 untuk beberapa model kamera. Semakin tinggi *shutter speed-*nya, semakin sedikit cahaya yang masuk, alhasil cocok untuk foto dalam kondisi terang atau cerah; semakin rendah *shutter*-nya, semakin banyak cahaya yang masuk.

Nah, jadi sekarang kalian sudah tahu 3 elemen terpenting dari segitiga eksposur — silakan berkreasi dengan ketiga elemen tersebut sesuka kalian.

Terima kasih!

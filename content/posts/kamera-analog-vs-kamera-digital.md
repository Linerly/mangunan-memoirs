---
title: Kamera Analog vs. Kamera Digital
date: 2024-02-17T16:01:22.000Z
featured_image: /img/nordwood-themes-f3dde_9thd8-unsplash.jpg
featured_image_attribution: 'Foto oleh <a
  href="https://unsplash.com/@nordwood?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">NordWood
  Themes</a> dari <a
  href="https://unsplash.com/photos/black-and-gray-film-camera-near-printed-photos-F3Dde_9thd8?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   '
draft: false
author: Alle
class: 7A
profile: https://www.instagram.com/filemkebakaran
profile_platform: instagram
---

> *Apa itu kamera analog, dan apa kekurangan kelebihannya dengan kamera digital?*

Sebelum kita ke sana, mari kita menguak apa itu kamera analog, dan jenis-jenisnya:

**Kamera analog** adalah kamera yang menggunakan kertas film bukan sensor elektronik biasanya terdiri dari 24-36 kali jepret (format 35mm) hingga 10-16 jepret (format 120mm).

Hasil dari kamera analog cenderung terpengaruh oleh lensa yang digunakan serta pencahayaan, setiap hasil bisa berbeda-beda warna dikarenakan kertas film (rol film) apa yang dipakai dan kesensitivitas cahayanya.

Kamera analog cenderung memiliki banyak variasi, seperti **SLR**, **TLR**, **Pocket**, dan **Rangefinder**; variasi-variasi tersebut memiliki tingkatan yang berbeda tergantung merek apa yang dipakai, untuk level pemula rata-rata disarankan untuk memakai kamera jenis Pocket dikarenakan penggunaannya yang cukup mudah tinggal masukkan kertas film dan jepret tidak perlu atur-atur segala.

Untuk yang mau benar-benar belajar, disarankan untuk menggunakan **SLR** hingga **Rangefinder**; kamera jenis ini mempunyai kontrol manual sehingga seberapa cahaya yang masuk bisa diatur dengan baik sehingga foto tidak terlalu gelap atau terang. Perbedaan dari ketiga tersebut bahwa **SLR** (*Single Lens Reflex*) memiliki **viewfinder** yang langsung terarah ke lensa jadi kalau masih ada penutup lensa kita tidak lupa lagi untuk melepasnya lalu **SLR** cenderung **interchangeable lens** dikarenakan lensanya bisa diganti-ganti tergantung mount kameranya; yang paling menarik dari kamera ini yaitu ada settingan **Shutter Speed**, **Aperture**, ISO dan pengaturan manual lainnya, lalu ada juga **Kokang**, **Lightmeter**, **Kokang Double Exposure**, **Depth of Field Viewer**, bahkan timer, lo. Yang kedua, **TLR (Twins Lens Reflex)** kamera ini kontrolnya seperti **SLR** tapi yang bedanya yaitu ada dua lensa, satu di atas untuk **viewfinder**, kedua di bawah untuk masuknya cahaya. Kamera ini menggunakan kertas film yang berukuran 120mm, bukan 35mm seperti **SLR** atau **Pocket**.

Yang terakhir yaitu **Rangefinder**, kamera ini seperti namanya yaitu Range Finder dan kamera ini cukup sulit untuk para pemula yang mau benar-benar belajar karena settingannya yang sulit diatur serta fokusnya yang agak menganggu bagi para pemula. Tetapi, jika kalian sudah mahir di manual... maka kamera ini cocok bagi kalian yang mau mendalami fotografi analog lebih dalam. Kamera ini biasanya terdiri dari **Aperture**, **ISO**, **Focusing**, **Lightmeter**, **Kokang**, dan **Battery Checker**. Beberapa kamera **rangefinder** tidak bisa mengontrol shutter speed dikarenakan lensanya yang kecil dan bagian atasnya yang cukup sempit.

Lalu hal-hal yang ditunggu, apa perbedaanya serta kelebihan kekurangan masing-masing?

Untuk Kelebihan kamera analog, kamera analog yang bertipe **SLR**, **TLR**, Beberapa **Rangefinder** bisa digunakan tanpa baterai sehingga jika kelupaan masih bisa untuk memakainya alias full mekanikal, peran baterai disitu adalah untuk pengukur cahayanya alias Lightmeter. Serta gambar yang dihasilkan cukup bisa dibilang bagus dan indah tergantung rol film apa yang dipakai tapi rata-rata setiap foto masih memiliki makna tersendiri, lalu jika kita memotret dengan analog setiap momen yang kita ingin potret kita bisa menghargai dan memikirkannya dua kali karena satu foto sama dengan satu botol fruittea. Tetapi, kekurangannya yaitu bahwa kertas film untuk kamera analog cukup mahal untuk zaman ini, dengan kisaran Rp165.000-180.000 atau 36 jepretan jika kalian mau yang bagus. Lalu tempat cuci dan scan kertas film, tetapi untuk zaman sekarang rata-rata di tiap kota sudah ada tempat cuci dan scan kertas film; tapi harga jasanya yang cukup agak mahal dikit di kisaran Rp50.000 sama scan (tergantung tipe film dan ukuran).

---

Sekarang kita beralih ke kelebihan kamera digital, hasil yang dihasilkan kamera digital cukup jernih dan bagus seperti kamera analog, lalu kita tidak perlu beli rol film setiap mau motret, tinggal beli kartu memori dan baterai kamera siap untuk *hunting*. Lalu, variasi dan harga dari kamera digital cukup bervariasi dan kadang-kadang bisa dapat yang murah bahkan Rp1.000.000 pun bisa dapat. Tetapi, kekurangannya yaitu karena kamera ini menggunakan baterai, jadi kalian harus cek sisa baterai agar jika dibawa buat *hunting* bisa dipakai tanpa harus kehilangan momen itu. Lalu, biaya *upgrade*-nya yang cukup mahal karena lensanya bisa berjuta-jutaan, belum termasuk aksesoris lainnya.

Jadi itulah kelemahan dan kelebihan dari masing-masing pihak, perlu di-*note* bahwa ini hanya informasi yang saya dapat dari berbagai pihak serta pengalaman saya sendiri menggunakannya!

Sekian, terima kasih.

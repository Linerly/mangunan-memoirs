---
title: Sejarah Berdirinya Perusahaan ASUS
date: 2024-03-22T03:07:14.246Z
featured_image: /img/rubaitul-azad-olkc61hyteo-unsplash.jpg
featured_image_attribution: Foto oleh <a
  href="https://unsplash.com/@rubaitulazad?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Rubaitul
  Azad</a> dari <a
  href="https://unsplash.com/photos/graphical-user-interface-OLKC61hyteo?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
draft: false
author: Yudhis
class: 9B
profile: https://www.instagram.com/andra.gotptsd
profile_platform: instagram
---
Pada tahun 1989, sebuah perusahaan bernama ASUS didirikan. Perusahaan tersebut didirikan oleh 4 insinyur komputer, yaitu Wayne Hsieh, Ted Hsu, MT Liao, dan TH Tung. Keempatnya memiliki visi dan misi yang sama untuk melanjutkan visi dan misi untuk memajukan industri IT di Taiwan. Nama *ASUS* dipilih dengan mengambil nama *pegasus*, yang kemudian mengambil huruf belakang nama tersebut, *asus*. Pada awalnya, sejarah ASUS dimulai di sebuah apartemen kecil di Taipei, Taiwan. Awalnya, ASUS hanyalah jasa konsultasi komputer, belum memproduksi *hardware* (perangkat keras). Mulai masuk dekade 80-an, perusahaan ini mulai menjual konstruksi *hardware* dan berhasil menjual *motherboard* PC untuk prosesor Intel 486.



ASUS mulai menjadi publik pada tahun 2005, di mana mereka menjual *motherboard* dan juga *notebook* secara komersil dan luas. Di era ini juga pada tahun 2007, ASUS membuat seri legendaris yang dikenal banyak orang, yakni seri laptop gaming ROG yang dikenal dan dicintai oleh kalangan *gamers*. Hingga sekarang, ASUS masih berjualan komponen PC, laptop, dan juga ponsel dengan spek yang gahar. Saat ini, ASUS terus selalu melakukan inovasi dan mengeluarkan produk-produk komputer gaharnya.

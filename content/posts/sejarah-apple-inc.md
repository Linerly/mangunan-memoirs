---
title: Sejarah Apple Inc.
date: 2024-03-15T01:11:00.000Z
featured_image: /img/sumudu-mohottige-bigpii04uig-unsplash.jpg
featured_image_attribution: Foto oleh <a
  href="https://unsplash.com/@stm_2790?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Sumudu
  Mohottige</a> dari <a
  href="https://unsplash.com/photos/apple-logo-on-blue-surface-bIgpii04UIg?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
draft: false
author: Yudhis
class: 9B
profile: https://www.instagram.com/andra.gotptsd
profile_platform: instagram
---
Kalian pasti tahu iPhone, kan? iPhone adalah sebuah ponsel pintar yang saat ini menjadi salah satu yang terbaik dan laris di pasaran. iPhone biasanya dijual dengan harga yang bervariasi, mulai dari 8 juta hingga ada yang sampai 25 juta rupiah di Indonesia. Namun, apakah kalian tahu perusahaan yang menciptakan iPhone? Perusahaan yang menciptakan sekaligus menjual iPhone adalah Apple Inc. Apple adalah sebuah perusahaan teknologi digital yang berasal dari Sillicon Valley, California. Perusahaan ini juga memproduksi dan menjual perangkat-perangkat lain seperti MacBook, iMac, iPad, dan bahkan dikabarkan sedang mengembangkan mobil listrik. Tetapi, apakah kalian tahu bagaimana sejarah dari awal perusahaan tersebut? Mari kita ulas.

# Sejarah

Pada tahun 1976, Steve Jobs, Steve Wozniak, dan Ronald Wayne mendirikan perusahaan Apple yang diberi nama Apple Computer Co. dengan pendanaan dari manajer pemasaran produk dan teknisi semi-pensiun Intel, A.C. Mike Markkula Jr.

Sebelum mendirikan Apple, Steve Jobs dan Steve Wozniak telah bersahabat lama. Steve Jobs berusaha membuat Wozniak tertarik untuk merakit komputer dan menjualnya. Komputer pertama yang diperkenalkan Jobs dan Wozniak diberi nama `Apple I`. Disusul pada tahun 1977, Jobs dan Wozniak memperkenalkan `Apple II`. Pada tahun 1980, Apple Computer Co. memperkenal `Apple III`, namun ternyata perkembangannya tidak sebaik saat memperkenalkan `Apple II`.

Pada tahun 1983, sebagai pendiri Apple, Steve Jobs menggandeng John Sculley dari perusahaan PepsiCo untuk memimpin Apple Computer. Akhirnya, pada tahun 1984, Apple memperkenalkan produk komputer Macintosh, yang merupakan komputer pertama yang berhasil dijual ke pasaran dengan menghadirkan fitur antarmuka pengguna grafis. Kesuksesan Macintosh inilah yang menjadikan Apple mulai menelantarkan `Apple II` demi mengembangkan produksi Mac.

Lambat laun, Jobs menyadari ada banyak hal yang kurang sejalan antara dia dan Sculley, sampai akhirnya pada tahun 1985, Sculley mengeluarkan Steve Jobs dari perusahaan Apple. Jobs kemudian mendirikan perusahaan baru yakni NeXT Computer yang bergerak dalam pengembangan software komputer. Pada 1996, Apple membeli NeXT seharga AS$402 juta dan secara otomatis membawa Jobs kembali ke perusahaan yang ia dirikan sendiri.

Di bawah bimbingan Jobs, Apple bisa meningkatkan penjualannya setelah memperkenalkan iMac sebagai komputer pertama yang dijual dengan mengutamakan penampilan.

Pada tahun 2007, Apple membuat suatu hal yang menggemparkan dunia waktu itu, di mana Steve Jobs berhasil menciptakan telepon genggam yang memiliki layar bisa disentuh (*touchscreen*). Ponsel tersebut bisa memutar video, memotret objek dengan kamera yang terpasang, memutar musik, dan bahkan bermain gim video. Ponsel tersebut adalah iPhone generasi pertama yang diluncurkan pada tahun 2007.

Mengutip dari buku *The Exclusive Biography* oleh Walter Isaacson (2011), pada Agustus 2011, Jobs mengundurkan diri sebagai CEO Apple meskipun tetap menjabat sebagai ketua dewan perusahaan. Tak lama kemudian, kesehatan Jobs diberitakan semakin mengalami penurunan.

Jobs melakukan cuti medis sejak Januari 2011 hingga akhirnya meninggal dunia pada tanggal 5 Oktober 2011 di California pada usia 56 tahun dengan diagnosis kanker pankreas. Pemakaman Steve Jobs diadakan secara tertutup pada tanggal 7 Oktober 2011.

# Penutup 

Itulah sejarah Apple dari awal perusahaan tersebut berdiri, semoga dari sejarah perusahaan tersebut membuat kita terinspirasi untuk bisa membuat hal yang revolusioner dan inovatif seperti perusahaan tersebut.

## Referensi
- kumparan.com

---
title: Pengertian Ultrabook
date: 2024-03-01T01:32:00.000Z
featured_image: /img/dell-ynvvnpcurd8-unsplash.jpg
featured_image_attribution: Foto dari <a
  href="https://unsplash.com/@dell?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Dell</a>
  dari <a
  href="https://unsplash.com/photos/laptop-on-brown-wooden-table-yNvVnPcurD8?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
draft: false
author: Yudhis
class: 9B
profile: https://www.instagram.com/andra.gotptsd
profile_platform: instagram
---
Pernahkah kamu membayangkan jika ada laptop yang di bawah 2 kg, enteng, kencang, performa setara dengan desktop, dan juga efisiensi?

Laptop itu berjenis *ultrabook*, *ultrabook* benar-benar ada di dunia nyata. *Ultrabook* adalah jenis laptop yang dirancang untuk memberikan kinerja yang tinggi dalam desain yang tipis dan ringan. Mereka biasanya memiliki baterai yang tahan lama, prosesor yang kuat, dan penyimpanan berbasis SSD untuk meningkatkan kecepatan dan responsivitasnya. *Ultrabook* biasanya dilirik oleh kaum pebisnis, dan juga pekerja kantoran yang membutuhkan laptop yang efisien, cepat dan ringkas.

*Ultrabook* ini biasanya menggunakan prosesor seri U. Seri U adalah prosesor yang berdaya rendah sekitar 15 watt, tetapi memiliki performa yang tidak bisa dianggap remeh. *Ultrabook* juga memakai SSD tipe NVMe M.2 yang super kencang dan ringkas dalam mengolah dan membaca data. *Ultrabook* biasanya bermaterial kokoh seperti metal, aluminium, dan ada juga yang memakai besi pesawat, contohnya Acer Swift 5 Aerospace. Biasanya laptop-laptop ini menggunakan prosesor Intel maupun AMD.

Untuk Intel sendiri, Intel bahkan melabeli suatu *ultrabook* dengan label EVO. EVO adalah label dari Intel untuk *ultrabook* yang beratnya di bawah 1,5 kg, memiliki USB 4/Thunderbolt, SSD super cepat, dan juga baterai yang tahan hingga minimal 10 jam.

*Ultrabook* sendiri biasanya dijual dari belasan hingga 20 juta, namun ada *ultrabook* di harga 7-10 jutaan. Di *range* harga ini, semua *ultrabook* ini memakai prosesor AMD seperti Acer Swift Go 14, dan satu *ultrabook* aneh yang menggunakan prosesor untuk performa tinggi, yakni Advan WorkPlus. Di saat *ultrabook* lain menggunakan prosesor seri U, laptop ini menggunakan prosesor seri H yang notabene processor performa tinggi untuk *gaming*, *editing*, *rendering*, dll.

Sekian saja untuk pengertian tentang *ultrabook,* terima kasih sudah membaca.

---
title: Shutter Count dalam Kamera
date: 2024-02-23T01:31:51.000Z
featured_image: /img/mechanical-shutter.jpg
featured_image_attribution: Foto © <a href="https://www.nikon.com">Nikon</a>
draft: false
author: Edo
class: 7B
profile: https://www.instagram.com/adrianus_tornado_
profile_platform: instagram
---

Dalam membeli sebuah produk baik baru ataupun bekas, kamu harus memperhatikan setiap detail dari produk tersebut agar tidak kecewa dikemudian hari. Sebagai contoh, membeli mobil bekas. Kamu tentu akan mengecek jarak tempuh mobil bekas tersebut. Dalam kamera, kamu juga harus mengetahui **Shutter Count** untuk mengetahui kualitas kamera. Lalu apa itu **Shutter Count**?

**Shutter Count (SC)** merupakan jumlah berapa kali tombol *shutter* pada kamera ditekan atau dipencet sehingga kamera menghasilkan sebuah foto yang dihitung dari saat penggunaan kamera pertama kali sampai saat terakhir kali kamera digunakan untuk mengambil gambar.

Lalu, kenapa kamu perlu mengetahui atau menghitung *shutter count* sebuah kamera **DSLR**?

Dengan mengetahui jumlah *shutter* yang digunakan, kamu bisa mengetahui sejauh mana kamera bisa bekerja dengan baik. Seperti yang diterangkan di atas, *shutter count* ibarat jumlah kilometer yang ditempuh sebuah mobil. Maka dari jumlah kilometer mobil itulah, kamu bisa mengetahui seberapa kerja keras mesin mobil bekerja. Hal ini juga berlaku pada perangkat kamera. Semakin banyak *shutter count* maka dipastikan kamera sudah bekerja keras. Pasalnya, ketika kamu menekan tombol *shutter* maka fungsi mekanis pada kamera juga bekerja hingga menghasilkan gambar.

Padahal setiap komponen kamera memilih batasan maksimal pemakaian. Perlu diperhatikan, kamera DSLR yang baik memiliki standar shutter count sekitar 100.000 kali. Hal ini berarti kamu bisa menghasilkan sekitar 100.000 foto dalam sebuah kamera DSLR tanpa adanya gangguan pada komponen kamera karena komponen dalam kondisi prima.

Jadi, jumlah *shutter count* sebuah kamera, bisa menjadi patokan atau bahan pertimbangan kamu saat membeli kamera bekas. Dengan mengetahui *shutter count*, kamu bisa mengetahui batas maksimal kamera bekas ini bekerja dengan baik.

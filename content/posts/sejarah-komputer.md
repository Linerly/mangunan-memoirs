---
title: Sejarah Komputer
date: 2024-02-16T02:06:55.000Z
featured_image: /img/ugi-k-anaucgs2fqe-unsplash.jpg
featured_image_attribution: Foto oleh <a
  href="https://unsplash.com/@wizzyfx?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Ugi
  K.</a> dari <a
  href="https://unsplash.com/photos/black-and-white-ip-desk-phone-on-brown-wooden-desk-anaUCgS2fqE?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
draft: false
author: Yuda
class: 9C
profile: https://www.instagram.com/coolvtrexbone
profile_platform: instagram
---

# Apa itu komputer?
Komputer adalah peralatan elektronik yang dapat gunakan untuk berbagai suatu hal, seperti meng-inputdata dan mengolahnya sesuai kebutuhan pengguna, serta menghasilkan keluaran berupa informasi dalam berbagai tampilan, seperti teks, gambar, audio, video, maupun audio-visual. Selain itu, anda dapat menggunakan komputer untuk menulusuri internet, menonton video, berselancar ke media sosial, bermain permainan video, dsb. Lalu, dari mana asal mula komputer dibuat pertama kalinya? Mari dengarkan!

Pada titik awal revolusi komputasi, abad ke-19 menjadi panggung bagi pergeseran paradigmatik yang dipimpin oleh seorang visioner bernama Charles Babbage. Dengan visi yang mencengangkan, Babbage berusaha menciptakan sebuah entitas mekanik yang melampaui batas-batas pemikiran pada zamannya. Terlahirlah Mesin Analitikal, cikal bakal dari perkembangan teknologi yang membawa manusia ke era baru dalam peradaban digital.
Secara umum, evolusi komputer dapat dibagi menjadi empat generasi yang mencerminkan kemajuan teknologi dan arsitektur komputasi:

1. Generasi pertama komputer dimulai dengan penerapan komputasi dalam lingkup akademik dan militer, seperti dengan pembuatan komputer Atanasoff-Berry pada tahun 1937 untuk menyelesaikan sistem persamaan linear, serta pembuatan komputer Colossus untuk memecahkan kode rahasia Reich Jerman.
Lalu, pada tahun 1946, lahir ENIAC sebagai tonggak penting dalam sejarah komputasi sebagai komputer pertama yang dirancang untuk tujuan umum. Saat ENIAC dinyalakan untuk pertama kalinya, insiden mati listrik di Philadelphia terjadi, menyadarkan dunia akan daya dan potensi revolusioner komputasi. Ditenagai oleh tabung vakum yang besar dan memakan banyak ruang, komputer generasi ini menjadi simbol awal dari kemajuan teknologi yang tak terelakkan.

2. Generasi kedua komputer dimulai dengan peralihan penting dari tabung vakum ke transistor, yang terbukti lebih kecil, lebih andal, dan lebih efisien secara energi.
Pada tahun 1951, UNIVAC memperkenalkan komputer pertama untuk penggunaan komersial, memulai era baru dalam industri komputer. Kemudian, pada tahun 1953, IBM memasuki bisnis komputer dengan merilis IBM 650 dan IBM 700, menandai kehadiran besar perusahaan dalam panggung komputasi.
Pada masa ini, berbagai bahasa pemrograman mulai dikembangkan, dan komputer mulai dilengkapi dengan memori dan sistem operasi, membawa kemampuan dan kenyamanan yang lebih besar bagi para pengguna.

3. Generasi ketiga komputer dimulai saat teknologi transistor berkembang menjadi sirkuit terpadu, memungkinkan pembuatan komputer yang lebih kompleks dalam ukuran yang lebih kecil.
Inovasi penting dalam generasi ini adalah munculnya komputer mini, yang memainkan peran krusial dalam memengaruhi generasi komputer berikutnya. NASA menggunakan komputer generasi ini secara luas dalam Program Apollo, termasuk Komputer Bimbingan Apollo yang membantu mengendalikan Apollo Command/Service Module.
Digital Equipment Corporation (DEC) menempati posisi kedua di pasar komputer setelah IBM dengan produk-produknya seperti PDP dan VAX, yang turut menggerakkan kemajuan industri.
Generasi ini juga menjadi tonggak penting dalam pengembangan sistem operasi dengan munculnya Unix, yang akan menjadi salah satu sistem operasi yang paling berpengaruh dalam sejarah komputasi.

4. Generasi keempat komputer dimulai pada tahun 1970-an dengan penemuan MOSFET dan kemudian integrasi berskala besar, yang membuka jalan bagi pengembangan mikroprosesor pada awal dekade tersebut.
Kemunculan mikroprosesor ini menjadi titik tolak penting dalam sejarah komputer karena memungkinkan pembuatan komputer pribadi yang semakin kecil. Awalnya, komputer rumahan dan komputer meja menjadi populer berkat kemajuan ini.
Kemudian, teknologi terus berkembang dengan lahirnya laptop dan ponsel cerdas, yang kemudian menjadi fenomena global. Perkembangan ini memicu persaingan sengit antara perusahaan teknologi dalam perlombaan paten atas ponsel cerdas dan inovasi terkait.

Terima kasih!
